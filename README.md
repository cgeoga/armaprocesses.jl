# ARMAProcesses.jl

A very simple package for working with ARMA processes. The package provides a
struct that contains the ARMA coefficients (and separately stores the
corresponding AR and MA polynomials) and acts as an infinite iterator. Since
ARMA processes are really best thought of as being specified with respect to a
specific white noise sequence pair, this package requests that a function be
provided that supplies the white noise, making it very flexible in case you
would like an ARMA process that is driven by, for example, iid Rademacher
variables.

A simple usage example that provides a length-`n` realization:

````{julia}
using ARMAProcesses
ar_coefs   = [0.8, -0.3]
ma_coefs   = [0.3, -0.2, 0.1]
noise_fx() = noise_sig*randn()
process    = arma(ar_coefs, ma_coefs, noise_fx)
sample     = collect(Iterators.take(process, n))
````

There are several convenience functions that are implemented as well:
`iscausal`, `isinvertible`, `acgf_point` (the autocovoariance generating
function at a point), `sdf_point` and `analyticalsdf` (the spectral density,
parameterized with the 2pi _inside_ the complex exponential), and
`analyticalacf` (obtained via the FFT). 

The simulation/iteration is pretty efficient. I don't expect that another
implementation could do much better than this, although clearly it wouldn't be
that hard to do as well. The other things are not particularly inefficient, but
I also would not say that they are as fast or optimized as they could be.

## Installation:
If you don't plan to hack on the code, you could simply open Julia, press the
`]` key to enter the package management mode, and run
````{julia}
pkg> add https://bitbucket.org/cgeoga/ARMAProcesses.jl.git
````

If you want to hack on it, my advice would be to clone the repository and then
add the following line to your `~/.julia/config/startup.jl`:
````{julia}
push!(LOAD_PATH, "/path/to/your/cloned/repo/ARMAProcesses.jl/src/")
````

After doing either of those options, you can then use the package like an
official one. See the example files for a demonstration.


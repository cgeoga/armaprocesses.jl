
module ARMAProcesses

  export arma, iscausal, isinvertible, analyticalsdf, analyticalacf

  using Polynomials, FFTW, FunctionWrappers
  import FunctionWrappers: FunctionWrapper

  abstract type ARMAPoly end

  struct UnivariateARMAPoly{T} <: ARMAPoly
    arpol::Polynomial{T}
    mapol::Polynomial{T}
  end

  struct MultivariateARMAPoly <: ARMAPoly end

  mutable struct ARMA{T,Z,P,Q,R<:ARMAPoly}
    ztfxn::FunctionWrapper{Z, Tuple{}} # Noise (Z_t) generating function
    arcof::NTuple{P, T} # [ ϕ_1, ϕ_2, ..., ϕ_p ]
    macof::NTuple{Q, T} # [ θ_1, θ_2, ..., θ_q ]
    outpt::NTuple{P, Z} # [ X_{t-1}, X_{t-2}, ..., X_{t-p} ]
    noise::NTuple{Q, Z} # [ Z_{t-1}, Z_{t-2}, ..., Z_{t-q} ]
    polys::R
  end


  # convenient constructor:
  function arma(arcoef, macoef, zfxn)
    (T,P,Q) = (eltype(arcoef), length(arcoef), length(macoef))
    if isempty(arcoef)
      eltype(arcoef) == Any && throw(error("For empty coef lists, please provide type annotations."))
    end
    if isempty(macoef)
      eltype(macoef) == Any && throw(error("For empty coef lists, please provide type annotations."))
    end
    Z = typeof(zfxn())
    if length(arcoef) > 12 || length(macoef) > 12
      @warn "For p or q > 12, this code falls back to less optimized propagation.
      Consider modifying the source code if you need this long of memory." maxlog=1
    end
    if eltype(arcoef) <: Array
      @warn "For Vector-, Matrix-, and Tensor-valued time series, please consider
      using stack-allocated objects if possible, for example via the StaticArrays 
      package. See the example files for a demonstration." maxlog=1
    end
    arc = NTuple{length(arcoef), eltype(arcoef)}(arcoef)
    oz  = NTuple{length(arcoef), eltype(arcoef)}([zero(Z) for _ in eachindex(arcoef)]) 
    mac = NTuple{length(macoef), eltype(macoef)}(macoef)
    nz  = NTuple{length(macoef), eltype(macoef)}([zero(Z) for _ in eachindex(macoef)]) 
    haspoly = T<:Number
    if haspoly
      _arpol   = Polynomial(vcat(one(Z), -collect(arc)))
      _mapol   = Polynomial(vcat(one(Z),  collect(mac)))
      out_poly = UnivariateARMAPoly(_arpol, _mapol)
    else
      out_poly = MultivariateARMAPoly()
    end
    wrapped_noisefn = FunctionWrapper{Z, Tuple{}}(zfxn)  
    R   = typeof(out_poly)
    out = ARMA{T,Z,P,Q,R}(wrapped_noisefn, arc, mac, oz, nz, out_poly)
    [propg!(out) for _ in 1:10*max(length(arc),length(mac))] # burn-in
    return out
  end

  @inline function shiftpush(V::NTuple{N, T}, newv::T) where{T,N}
    if N == 1
      V = (newv,)
    elseif N == 2
      V = (newv, V[1])
    elseif N == 3
      V = (newv, V[1], V[2])
    elseif N == 4
      V = (newv, V[1], V[2], V[3])
    elseif N == 5
      V = (newv, V[1], V[2], V[3], V[4])
    elseif N == 6
      V = (newv, V[1], V[2], V[3], V[4], V[5])
    elseif N == 7
      V = (newv, V[1], V[2], V[3], V[4], V[5], V[6])
    elseif N == 8
      V = (newv, V[1], V[2], V[3], V[4], V[5], V[6], V[7])
    elseif N == 9
      V = (newv, V[1], V[2], V[3], V[4], V[5], V[6], V[7], V[8])
    elseif N == 10
      V = (newv, V[1], V[2], V[3], V[4], V[5], V[6], V[7], V[8], V[9])
    elseif N == 11
      V = (newv, V[1], V[2], V[3], V[4], V[5], V[6], V[7], V[8], V[9], V[10])
    elseif N == 12
      V = (newv, V[1], V[2], V[3], V[4], V[5], V[6], V[7], V[8], V[9], V[10], V[11])
    else
      V = tuple(newv, V[1:(N-1)]...)
    end
  end

  function propg!(X::ARMA{T,Z,P,Q,R}) where{T,Z,P,Q,R}
    newn = X.ztfxn()
    newv = copy(newn)
    if (P > 0 && Q > 0) 
      newv   += sum(prod, zip(X.arcof, X.outpt)) + sum(prod, zip(X.macof, X.noise))
      X.outpt = shiftpush(X.outpt, newv)
      X.noise = shiftpush(X.noise, newn)
      return newv
    elseif P > 0
      newv   += sum(prod, zip(X.arcof, X.outpt)) 
      X.outpt = shiftpush(X.outpt, newv)
      return newv
    elseif Q > 0 
      newv   += sum(prod, zip(X.macof, X.noise)) 
      X.noise = shiftpush(X.noise, newn)
      return newv
    else
      return newv
    end
  end

  Base.IteratorSize(::Type{<:ARMA}) = Base.IsInfinite()

  Base.IteratorEltype(::Type{<:ARMA}) = Base.EltypeUnknown()

  function Base.iterate(X::ARMA{T,Z,P,Q,R}, (el,idx)=(propg!(X), 0)) where{T,Z,P,Q,R} 
    (el, (propg!(X), idx+1))
  end

  function iscausal(X::ARMA{T,Z,P,Q,R}) where{T,Z,P,Q,R} 
    T <: Number || error("Currently only implemented for univariate processes.")
    iszero(P) && return true
    minimum(abs, roots(X.polys.arpol)) > 1.0
  end

  function isinvertible(X::ARMA{T,Z,P,Q,R}) where{T,Z,P,Q,R}
    T <: Number || error("Currently only implemented for univariate processes.")
    iszero(Q) && return true
    minimum(abs, roots(X.polys.mapol)) > 1.0
  end

  @inline function acgf_point(X::ARMA{T,Z,P,Q,R}, z) where{T,Z,P,Q,R} 
    T <: Number || error("Currently only implemented for univariate processes.")
    X.polys.arpol(z)*X.polys.arpol(1.0/z)
  end

  @inline function sdf_point(X::ARMA{T,Z,P,Q,R}, f, noise_sig) where{T,Z,P,Q,R}
    T <: Number || error("Currently only implemented for univariate processes.")
    out  = noise_sig^2
    out *= abs2(X.polys.mapol(exp(-im*2.0*pi*f)))
    out/abs2(X.polys.arpol(exp(-im*2.0*pi*f)))
  end

  function analyticalsdf(X::ARMA{T,Z,P,Q,R}, n, noise_sig; 
                         return_grid=false) where{T,Z,P,Q,R}
    T <: Number || error("Currently only implemented for univariate processes.")
    fgrid = range(-0.5, 0.5, length=n+1)[1:n]
    sdfv  = [sdf_point(X, f, noise_sig) for f in fgrid]
    return_grid ? (fgrid, sdfv) : sdfv
  end

  function analyticalacf(X::ARMA{T,Z,P,Q,R}, noise_sig; lag=20, n=6*lag) where{T,Z,P,Q,R}
    T <: Number || error("Currently only implemented for univariate processes.")
    lag > Int64(round(0.25*n)) && @warn("Please increase n for this many lags...")
    real(ifft(fftshift(analyticalsdf(X, n, noise_sig))))[1:lag]
  end

end


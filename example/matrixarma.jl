
using LinearAlgebra, StaticArrays, ARMAProcesses

# This provides a method to efficiently simulate a process of the form
#
# X_t = P1 X_{t-1} + P_2 X_{t-2} + P_3 X_{t-3} + Z_t + T_1 Z_{t-1} + T_2 Z_{t-2}
#

# The key to good performance here is to tell the compiler the size of your
# matrices ahead of time. If you tell the compiler that your process will be 3x3
# matrices, for example, it knows exactly how many bits to allocate and can do a
# lot of optimizations that it otherwise couldn't do. So we're going to do a
# little extra work to make all that information known to the compiler. As a
# result, the matrix ARMA process will barely be slower to simulate than the
# scalar-valued one.
I3       = Matrix(I, 3, 3)
static_I = SMatrix{3,3}(I3)
Phi1     = static_I.*0.8    # The ".*" syntax everywhere here is broadcasted
Phi2     = static_I.*-0.3   # multiplications. In this case, that means that
Phi3     = static_I.*0.1    # every element of the matrix static_I is being
Theta1   = static_I.*0.5    # multiplied by that number. I'm just doing this 
Theta2   = static_I.*-0.1   # simple example so I know it will be causal and nice.

# The noise function. It is _especially_ important that this function be static,
# because it's going to get called a lot. And while I can actually fix
# non-static inputs for the AR or MA terms, I cannot fix a non-static noise
# function. I've implemented the code this way so that you can put in any white
# noise sequence you want, not just Gaussian.
@inline noise()  = @SMatrix(randn(3,3))

# Declare the ARMA process object:
process  = arma([Phi1, Phi2, Phi3], [Theta1, Theta2], noise)

# Now enjoy the structure of an infinite iterator:
sample1  = collect(Iterators.take(process, 100))
sample2  = collect(Iterators.take(process, 100))
#... etc


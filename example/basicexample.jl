
using ARMAProcesses

# AR coefficients:
arc = [0.95, -0.3, -0.2]

# MA coefficients:
mac = [-0.75, 0.2]

# Noise generating function:
noise_sigma      = 1.0
noise_function() = randn()

# the ARMA structure:
process = arma(arc, mac, noise_function)

# A realization from this process:
n = 1024
data = collect(Iterators.take(process, n))

# The analytical spectral density of our process (with frequency 
# grid for convenience):
n_freq = 1024
(fgrid, spec) = analyticalsdf(process, n_freq, noise_sigma, return_grid=true)

# The analytical ACF up to lag 20:
n_lag = 20
acf   = analyticalacf(process, noise_sigma, lag=n_lag)


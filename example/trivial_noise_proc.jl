
using LinearAlgebra, StaticArrays, ARMAProcesses

# A demo of what to do if you have empty coefficient lists.

# The noise function. It is _especially_ important that this function be static,
# because it's going to get called a lot. And while I can actually fix
# non-static inputs for the AR or MA terms, I cannot fix a non-static noise
# function. I've implemented the code this way so that you can put in any white
# noise sequence you want, not just Gaussian.
@inline noise()  = @SMatrix(randn(3,3))

# Declare the ARMA process object:
const process_type = SArray{Tuple{3,3}, Float64, 2, 9}
process  = arma(process_type[], process_type[], noise)

# Now enjoy the structure of an infinite iterator:
sample1  = collect(Iterators.take(process, 100))
sample2  = collect(Iterators.take(process, 100))
#... etc
